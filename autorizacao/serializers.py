from rest_framework import serializers

from agendamento.models import Agendamento
from .models import Autorizacao
from responsavel.serializers import ReponsavelSerializer
from fila.serializers import FilaSerializer
from agendamento.serializers import AgendamentoSerializer

class AutorizacaoSerializer(serializers.ModelSerializer):
    responsavel = ReponsavelSerializer()
    filas = FilaSerializer(many=True)
    agendamento = AgendamentoSerializer()
    class Meta:
        model = Autorizacao
        fields = ('id', 'nome', 'responsavel', 'status', 'data', 'hora', 'filas', 'agendamento', 'create_fila')