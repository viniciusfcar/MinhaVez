from rest_framework import serializers
from .models import UnidadeSaude
from consulta.serializers import ConsultaSerializer
from autorizacao.serializers import AutorizacaoSerializer
from especialista.serializers import EspecialistaSerializer
from exame.serializers import ExameSerializer
from responsavel.serializers import ReponsavelSerializer

class UnidadeSaudeSerializer(serializers.ModelSerializer):
    consultas = ConsultaSerializer(many=True)
    autorizacoes = AutorizacaoSerializer(many=True)
    especialistas= EspecialistaSerializer(many=True)
    exames = ExameSerializer(many=True)
    responsaveis = ReponsavelSerializer(many=True)
    class Meta:
        model = UnidadeSaude
        fields = ('id', 'razao_social', 'cep', 'logradouro', 'numero', 'complemento', 'bairro', 'cidade', 'estado',
                  'consultas', 'autorizacoes', 'especialistas', 'exames', 'responsaveis', 'telefone', 'email')