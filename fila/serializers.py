from rest_framework import serializers
from .models import Fila
from ficha.serializers import FichaSerializer

class FilaSerializer(serializers.ModelSerializer):
    fichas = FichaSerializer(many=True)
    
    class Meta:
        model = Fila
        fields = ('id', 'nome', 'vagas', 'fila_preferencial', 'fichas', 'total_fichas')