from rest_framework import serializers
from .models import Especialista
from especializacao.serializers import EspecializacaoSerializer
from profissao.serializers import ProfissaoSerializer

class EspecialistaSerializer(serializers.ModelSerializer):
    profissao = ProfissaoSerializer()
    especializacao =  EspecializacaoSerializer(many=True)
    class Meta:
        model = Especialista
        fields = ('id', 'nome', 'sobrenome', 'profissao', 'especializacao', 'imagem')