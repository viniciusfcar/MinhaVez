from rest_framework import serializers
from .models import Consulta
from especialista.serializers import EspecialistaSerializer
from agendamento.serializers import AgendamentoSerializer
from fila.serializers import FilaSerializer

class ConsultaSerializer(serializers.ModelSerializer):
    especialista = EspecialistaSerializer()
    agendamento = AgendamentoSerializer()
    filas = FilaSerializer(many=True)

    class Meta:
        model = Consulta
        fields = ('id', 'nome', 'especialista', 'data', 'hora', 'filas', 'status', 'agendamento', 'create_fila')