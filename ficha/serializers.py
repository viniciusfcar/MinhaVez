from rest_framework import serializers
from usuario.serializers import UsuarioSerializer
from .models import Ficha

class FichaSerializer(serializers.ModelSerializer):
    usuario = UsuarioSerializer()
    class Meta:
        model = Ficha
        fields = ('id', 'numero', 'usuario', 'preferencial', 'status')