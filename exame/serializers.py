from rest_framework import serializers
from .models import Exame
from fila.serializers import FilaSerializer
from agendamento.serializers import AgendamentoSerializer

class ExameSerializer(serializers.ModelSerializer):
    filas = FilaSerializer(many=True)
    agendamento = AgendamentoSerializer()
    class Meta:
        model = Exame
        fields = ('id', 'nome', 'tipo', 'status', 'data', 'hora', 'filas', 'agendamento', 'create_fila')